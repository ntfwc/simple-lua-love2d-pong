# Description
A simple clone of pong.

This is a Lua version of the C++ Pong clone I made earlier using the LÖVE game engine. This version only took about 5 1/2 hours to make.

# Features
* Two independently controlled paddles
* A ball with some physics
* Score tracking

# Dependencies
* [LÖVE](https://love2d.org/)

# Running
For the source, run the LÖVE executable on the src directory.

For the release, just pass the .love file to LÖVE. On most systems this can be done by dragging the file to the LÖVE executable. If LÖVE was installed then it may be associated with the file and then you can just double-click it.

# Controls

Player 1:

* w = move up
* s = move down

Player 2:

* arrow up = move up
* arrow down = move down

-- Provides methods compatible for multiple Love2D versions

-- luacheck: globals Compat
Compat = {}

-- luacheck: globals love.graphics.point love.graphics.points
if love.graphics.point then
  function Compat.draw_point(x, y)
    love.graphics.point(x, y)
  end
else
  function Compat.draw_point(x, y)
    love.graphics.points(x, y)
  end
end

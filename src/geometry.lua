-- luacheck: globals Geometry
Geometry = {}

function Geometry.find_rectangle_intersection(rectangle1, rectangle2)
  local mx1 = math.max(rectangle1.x, rectangle2.x)
  local my1 = math.max(rectangle1.y, rectangle2.y)
  local mx2 = math.min(rectangle1.x + rectangle1.w, rectangle2.x + rectangle2.w)
  local my2 = math.min(rectangle1.y + rectangle1.h, rectangle2.y + rectangle2.h)
  local result = { x=mx1, y=my1, w=mx2 - mx1, h=my2 - my1 }
  if result.w <= 0 or result.h <= 0 then
    return nil
  end
  return result
end

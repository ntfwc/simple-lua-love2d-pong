-- luacheck: read_globals Geometry
require "geometry"

-- luacheck: read_globals Paddle
require "paddle"

-- luacheck: globals Ball
Ball = {
  initial_x = 0,
  initial_y = 0,
  x = 0,
  y = 0,
  velocity = { x = 0, y = 0 }
}

local INITIAL_VELOCITY_X = 3
local INITIAL_VELOCITY_Y = 3
Ball.SIZE = 10

local MAX_X_SPEED = 7

function Ball:new(x, y)
  local o = {
    initial_x = x,
    initial_y = y,
    x = x,
    y = y,
    velocity = { x = INITIAL_VELOCITY_X, y = INITIAL_VELOCITY_Y }
  }
  setmetatable(o, self)
  self.__index = self
  return o
end

function Ball:handle_paddle_collision(intersection, is_left_paddle)
  local width = intersection.w
  if not is_left_paddle then
    width = -width
  end
  self.x = self.x + width * 2

  local new_x_speed = -(self.velocity.x + (self.velocity.x > 0 and 1 or -1))
  self.velocity.x = new_x_speed
  if self.velocity.x > MAX_X_SPEED then
    self.velocity.x = MAX_X_SPEED
  end
end

function Ball:update(screen_dimensions, paddle1, paddle2)
  self.x = self.x + self.velocity.x
  self.y = self.y + self.velocity.y

  local function flip_y(new_y)
    self.y = new_y
    self.velocity.y = -self.velocity.y
  end

  local max_y = screen_dimensions.height - Ball.SIZE

  if self.y < 0 then
    flip_y(-self.y)
  else if self.y > max_y then
    flip_y(2 * max_y - self.y)
  end
  end

  local function reset()
    self.x = self.initial_x
    self.y = self.initial_y
    local was_going_right = self.velocity.x > 0
    self.velocity = { x = INITIAL_VELOCITY_X, y = INITIAL_VELOCITY_Y }
    if was_going_right then
      self.velocity.x = -self.velocity.x
    end
  end

  local function handleListener(listener)
    if listener then
      listener()
    end
  end

  if self.x < -Ball.SIZE then
    reset()
    handleListener(self.exitLeftListener)
    return
  end

  if self.x > screen_dimensions.width then
    reset()
    handleListener(self.exitRightListener)
    return
  end

  local function find_intersection(paddle)
    local rectangle1 = { x = self.x, y = self.y, w = Ball.SIZE, h = Ball.SIZE }
    local rectangle2 = { x = paddle.x, y = paddle.y, w = Paddle.WIDTH, h = Paddle.HEIGHT }
    return Geometry.find_rectangle_intersection(rectangle1, rectangle2)
  end

  local intersection = find_intersection(paddle1)
  if intersection then
    self:handle_paddle_collision(intersection, true)
  else
    intersection = find_intersection(paddle2)
    if intersection  then
      self:handle_paddle_collision(intersection, false)
    end
  end
end

function Ball:draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.rectangle('fill', self.x, self.y, Ball.SIZE, Ball.SIZE)
end

function Ball:setExitLeftListener(exitLeftListener)
  self.exitLeftListener = exitLeftListener
end

function Ball:setExitRightListener(exitRightListener)
  self.exitRightListener = exitRightListener
end

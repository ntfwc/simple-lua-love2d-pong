-- luacheck: read_globals Paddle
require "paddle"

-- luacheck: read_globals Compat
require "compat"

-- luacheck: read_globals Ball
require "ball"

-- luacheck: read_globals Score
require "score"

local SCREEN_DIMENSIONS = { width = 480, height = 360 }
local Main = {}

function love.load()
  Main.paddle1 = Paddle:new(20, SCREEN_DIMENSIONS.height)
  Main.paddle2 = Paddle:new(SCREEN_DIMENSIONS.width - 30, SCREEN_DIMENSIONS.height)
  Main.ball = Ball:new(SCREEN_DIMENSIONS.width / 2 - Ball.SIZE / 2, SCREEN_DIMENSIONS.height / 2 - Ball.SIZE / 2)
  Main.score1 = Score:new(SCREEN_DIMENSIONS.width / 2 - 100, 20)
  Main.score2 = Score:new(SCREEN_DIMENSIONS.width / 2 + 100, 20)

  Main.ball:setExitRightListener(function()
    Main.score1:increment()
  end)
  Main.ball:setExitLeftListener(function()
    Main.score2:increment()
  end)
end

function love.update(dt)
  local function handle_paddle_input(up_key, down_key, paddle)
    if love.keyboard.isDown(up_key) then
      paddle:move_up()
    end
    if love.keyboard.isDown(down_key) then
      paddle:move_down(SCREEN_DIMENSIONS.height)
    end
  end

  handle_paddle_input("w", "s", Main.paddle1)
  handle_paddle_input("up", "down", Main.paddle2)

  Main.ball:update(SCREEN_DIMENSIONS, Main.paddle1, Main.paddle2)
end

function love.draw()
  Main.paddle1:draw()
  Main.paddle2:draw()

  local function draw_dividing_line(x)
    local x_shifted = x + 0.5
    love.graphics.setColor(255, 255, 255, 255)
    for y = 0.5, SCREEN_DIMENSIONS.height, 4 do
      Compat.draw_point(x_shifted, y)
    end
  end

  draw_dividing_line(SCREEN_DIMENSIONS.width / 2)
  Main.ball:draw()
  Main.score1:draw()
  Main.score2:draw()
end

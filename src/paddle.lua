-- luacheck: globals Paddle
Paddle = {
  x = 0,
  y = 0,
}

Paddle.WIDTH = 10
Paddle.HEIGHT = 40
local PADDLE_SPEED = 4

function Paddle:new(x, screen_height)
  local o = { x=x, y= screen_height / 2 - Paddle.HEIGHT / 2 }
  setmetatable(o, self)
  self.__index = self
  return o
end

function Paddle:draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.rectangle('fill', self.x, self.y, Paddle.WIDTH, Paddle.HEIGHT)
end

function Paddle:move_down(screen_height)
  self.y = self.y + PADDLE_SPEED

  local max_y = screen_height - Paddle.HEIGHT
  if self.y > max_y then
    self.y = max_y
  end
end

function Paddle:move_up()
  self.y = self.y - PADDLE_SPEED

  if self.y < 0 then
    self.y = 0
  end
end

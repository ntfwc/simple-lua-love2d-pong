-- luacheck: globals Score
Score = {
  x = 0,
  y = 0,
  value = 0
}

local MAIN_FONT = love.graphics.newFont(16)

function Score:new(x, y)
  local o = { x = x, y = y }
  setmetatable(o, self)
  self.__index = self
  return o
end

function Score:draw()
  love.graphics.setFont(MAIN_FONT)
  love.graphics.print(self.value, self.x, self.y)
end

function Score:increment()
  self.value = self.value + 1
  if self.value > 1000 then
    self.value = 1000
  end
end
